// WHY DON'T FISH LIKE TO PLAY TENNIS
// BC THEY ARE AFRAID OF NETS

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html");
    var boob = bob[0];

    if(customize_page){
        boob.setAttribute("customize-page","true");
    } else if(on_main){
        boob.setAttribute("customize-page","false");
    }

    document.querySelectorAll("span[src-link], .meh-main a").forEach(uvu => {
        var vek = uvu.textContent;

        if(vek.indexOf("-") > -1){
            var lastdab = vek.lastIndexOf("-");
            var manydab = vek.substring(lastdab+1);
            if(manydab.substring(0,4) == "deac"){
                uvu.textContent = vek.substring(0,vek.lastIndexOf("-")) + " (deactivated)";
            }
        }
    });
});

// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');

// 'previous page' svg
// feathericons
var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";

document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');

// 'next page' svg
// feathericons
var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";

document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');

// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");

    $(".tumblr_preview_marker___").remove();

    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:69,
        attribute:"title"
    });

    /*------- REMOVE NPF VIDEO AUTOPLAY -------*/
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay");
        
        $(this).prop("muted",true);
        
        $(this).click(function(){
            if($(this).prop("muted")){
                $(this).prop('muted', false);
            } else {
                $(this).prop('muted', true);
            }
        });
    });

    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }

        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })

    $(".postinner p, .postinner blockquote, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }

        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })
    
    $(".postinner pre").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })

    // remove empty captions
    $(".caption").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })

    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })

    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });

            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);

    $(".albumwrap").click(function(){

        var emp = $(this).parents(".audiowrap").find("audio")[0];

        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }

        var that = this

        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })

    // minimal soundcloud player ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â© shythemes.tumblr
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Body-Text-Color");

    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });

    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       });
    }

    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })

    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();

        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }

            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })

        $(this).html(
            $(this).html().replace("(via","")
        )

        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })

        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }

            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }

            $(this).html($.trim($(this).html()));

            if($(this).text() == ")"){
                $(this).remove()
            }
        })

        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })

        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })

    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })

    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })

    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })

        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");

        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })

    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row)").andSelf().wrapAll('<div class="chatwrap el-npf-chat">');
            } else {
                $(this).nextUntil(":not(.chat_row)").addBack().wrapAll('<div class="chatwrap el-npf-chat">');
            }
        });
    })

    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })

    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })



    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Tags-Fade-Speed-MS"));


    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".botperma").prev(".postinner").find(".tagsdiv");

        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })

    /*----------- POST NOTES -----------*/
    $("ol.notes a[title]").hover(function(){
        $(this).removeAttr("title")
    });

    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });

    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();

    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();

            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;

            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;

            $(this).height(rat_h)
        }
    })

    /*--- fvck tvmblr ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","https://cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });

    /*----- OTHER -----*/
    if(customize_page){
        $(".reblog-head img").each(function(){
            if($(this).attr("src") == $("html[portrait]").attr("portrait")){
                $(this).remove()
            }
        })
        
        $("[post-type='text'] .reblog-comment").each(function(){
            $(this).find("p").eq(0).each(function(){
                if(!$(this).prev().length){
                    if($(this).next(".npf_inst").length){
                        if($.trim($(this).html()) == ""){
                            $(this).remove()
                        }
                    }
                }
            })
        })
        
        $(".reblog-comment p[last-comment]").each(function(){
            if($(this).children().first().is("a.tumblr_blog")){
                $(this).css("margin-top",0)
            }
        })
    }

    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    $(".postinner").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType === 3  && this.data.trim().length > 0
        }).wrap("<span/>")
    })

    /*--- UNWRAP THE BLOGTITLE THING (tumblr wtf lol) ---*/
    $(".tumblr_theme_marker_blogtitle").contents().unwrap();
    
    /*--- POST MENU & COPY LINK BUTTON ---*/
    var ex_txt = $(".meh-main [data-clipboard-text]").eq(0).text();
    
    $(".acorn").click(function(){
        
        if(!$(this).hasClass("nvt")){
            // reset all other button settings
            $(".acorn").removeClass("nvt");
            $(".acorn").next(".meh-menu").removeClass("swip").find("[data-clipboard-text]").text(ex_txt);
            $(".yepclique").removeClass("yepclique");
            
            // apply active class to the one that's clicked
            $(this).next(".meh-menu").addClass("swip");
            $(this).addClass("nvt")
        } else {
            // 'toggle' of self-click
            $(this).next(".meh-menu").removeClass("swip");
            $(this).removeClass("nvt")
        }
    })
    
    $(document).on("click",function(meeeh){
        if(!$(meeeh.target).closest(".acorn, .meh-menu").length){
            $(".meh-menu").removeClass("swip");
            $(".acorn").removeClass("nvt")
        }
    });
    
    /*----------------*/
    
    var clipboard = new ClipboardJS("[data-clipboard-text]");
    
    $("[data-clipboard-text]").hover(function(){
        $(this).attr("href","#")
    }, function(){
        $(this).removeAttr("href")
    });
    
    /*----------------*/
    
    $(".meh-main [data-clipboard-text]").click(function(){
        $(this).removeAttr("href");
        
        if(!$(this).hasClass("yepclique")){
            
            $(this).text("link copied!");
            
            $(this).addClass("yepclique");
            
            var lenz = $(".yepclique").length.toString();
            
            if(lenz > "1"){
                $(".meh-main [data-clipboard-text]").not($(this)).text(ex_txt)
            }
        } else {
            $(this).text("link copied!")
        }
        
    })
    
    /*--- SIDEBAR STUFF ---*/
    $(".main-sb-img").on("dragstart", function(ree){
        ree.preventDefault();
    });
    
    $(".chellfish").each(function(){
        $(this).html($.trim($(this).html()));
        var desun = $(this).html();
        desun = desun.replaceAll("[img]","<div class='avowrap'></div>")
                     .replaceAll("[msg]","<div top></div>")
                     .replaceAll("[icon-name]","<div bot><i class='iconsax'></i></div>")
                     .replaceAll("[sender]","<span sender></span>")
                     .replaceAll("***","<div class='notif-wrap'></div>")
        $(this).html(desun)
        
        $(this).contents().filter(function(){
            return this.nodeType === 3  && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $(".chellfish span").each(function(){
        $(this).html($.trim($(this).html()))
    })
    
    $(".notif-wrap").each(function(){
        $(this).nextUntil(".notif-wrap").prependTo($(this));
        $(this).wrapInner("<div class='notif-block'>")
    })
    
    // add the small image
    $(".avowrap").each(function(){
        var avsrc = $(this).next("span").text();
        if(avsrc.slice(0,1) == "[" && avsrc.substr(avsrc.length-1) == "]"){
            avsrc = avsrc.slice(1,-1);
            $(this).prepend("<img class='mini-avi' src='" + avsrc + "'>");
            $(this).css("background-image","url('" + avsrc + "')");
            $(this).next("span").remove()
        }
    })
    
    // get the msg text
    $(".notif-wrap [top]").each(function(){
        $(this).nextUntil("[bot]").prependTo($(this));
        $(this).wrap("<div class='notif-text'>");
        var ttnut = $(this).text();
        // check if first and last charas are []
        // if yes, remove the first instance of [ from HTML
        // if no, uh...
        if(ttnut.slice(0,1) == "[" && ttnut.substr(ttnut.length-1) == "]"){
            ttnut = ttnut.slice(1,-1);
            $(this).text(ttnut)
        }
    })
    
    // get the 'sender' text
    $(".notif-wrap [bot]").each(function(){
        $(this).nextAll().appendTo($(this));
        if($(this).prev().is(".notif-text")){
            $(this).appendTo($(this).prev())
        }
    });
    
    // get icon name
    $(".notif-block .iconsax").each(function(){
        $(this).attr("temp-name",$(this).next("span").text().replaceAll("[","").replaceAll("]",""));
        $(this).attr("icon-name",$(this).attr("temp-name"));
        $(this).next("span").remove();
    })
    
    init_iconsax();
    
    if($(".notif-wrap").length){
        $(".chellfish").css("overflow","initial")
    }
    
    // get sender info
    $("[sender]").each(function(){
        var plume = $(this).next("span").text();
        if(plume.slice(0,1) == "[" && plume.substr(plume.length-1) == "]"){
            plume = plume.slice(1,-1);
            $(this).text(plume);
            
            $(this).next("span").remove();
        }
    })
    
    // remove last [***] if it's empty
    $(".notif-wrap:last").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })
    
    // only display sidebar color-hover panel if
    // there are still notif blocks
    if($(".moolait").is("[sb-hov-yes]")){
        $(".paindesk").hover(function(){
            $(".moolait").addClass("mooed");
            
            if($(".calicow").length){
                if(!$(".calicow").eq(0).siblings(".notif-wrap:not(.calicow)").length){
                    $(".moolait").removeClass("mooed")
                }
            }
        }, function(){
            $(".moolait").removeClass("mooed")
        })
    }
    
    /*-- mplayer stuff --*/
    var bowe = getComputedStyle(document.documentElement)
               .getPropertyValue("--Sidebar-Width").replace(/\D/g, "");
               
    var quickie = getComputedStyle(document.documentElement)
               .getPropertyValue("--MusicPlayer-Text-Fade-Speed-RAW");
               
    var ap_first = getComputedStyle(document.documentElement)
               .getPropertyValue("--Autoplay-First-Song");
               
    var ap_next = getComputedStyle(document.documentElement)
               .getPropertyValue("--Autoplay-Next-Song"); 
               
    var pl_loop = getComputedStyle(document.documentElement)
               .getPropertyValue("--Loop-Playlist-Infinitely"); 
               
    if($(".botstick").length){
        // de-dropbox
        $(".audiostaque audio").each(function(){
            var mp3 = $.trim($(this).attr("src"));
            mp3 = mp3.replaceAll("?dl=0","").replaceAll("www.dropbox","dl.dropbox");
            $(this).attr("src",mp3)
        })
        
        /*--*/
        
        // if song text is too long, wrap it in a marquee
        $(".dingding span").each(function(){
            if($(this).width() > bowe * 0.9){
                $(this).wrap("<div class='marinah'></div>");
                $(this).parent().wrap("<span class='markee'></span>");
                $(this).contents().unwrap()
            }
        });
        
        // add track number to each instance of song text
        $(".dingding span").each(function(dong){
            dong += 1;
            $(this).attr("id","track-name-" + dong);
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }
        });
        
        
        // remove [autoplay] attr if user added it in the html
        $(".audiostaque [autoplay]").removeAttr("autoplay");
        
        $(".audiostaque audio:first").addClass("current-song");
        
        // if autoplay first song
        if(ap_first == "yes"){
            $(window).load(function(){
                var pute = $(".current-song")[0];
                if(pute.paused){
                    pute.play();
                    $(".vlay").hide();
                    $(".vause").show();
                } else {
                    pute.pause();
                    $(".vlay").show();
                    $(".vause").hide();
                }
            })
        }
        
        // initialize audios and their song names
        $(".audiostaque audio").each(function(ce){
            var that = this;
            
            ce += 1;
            $(this).attr("id","track-src-" + ce);
            
            /*---- VOLUME SETTINGS ----*/
            var vol = $(this).attr("vol");
            
            if(vol.indexOf("%")){
                vol = vol.substring(0,vol.lastIndexOf("%"));
                vol = vol / 100;
            }
            
            var cette = $(this).attr("id");
            cette = document.getElementById(cette);
            cette.volume = vol;
            /*-------------------------*/
            
            if($.trim($(this).attr("src")) == ""){
                $(this).remove();
            }
        });
        
        
        /*--- playpause button ---*/
        $(".qorqle").click(function(){
            var n__n = $(".current-song")[0];
    
            if(n__n.paused){
                n__n.play();
                $(".vlay").hide();
                $(".vause").show();
            } else {
                n__n.pause();
                $(".vlay").show();
                $(".vause").hide();
            }
        });//end click
        
        // when each song ends, jumps to the next one
        $(".audiostaque audio").each(function(){
            $(this).bind("ended", function(){
                if(ap_next == "yes"){
                    if($(this).next("audio").length){
                        $(".current-song").each(function(){
                            $(this).next("audio").addClass("current-song");
                            $(this).removeClass("current-song")
                        });
                        
                        var dulait = $(".current-song").attr("id");
                        dulait = dulait.replace(/\D/g, "");
                        
                        var n__n = $(".current-song")[0];
                
                        if(n__n.paused){
                            n__n.play();
                            $(".vlay").hide();
                            $(".vause").show();
                        } else {
                            n__n.pause();
                            $(".vlay").show();
                            $(".vause").hide();
                        }
                        
                        $(".dingding span").addClass("bongfade");
                        
                        setTimeout(function(){
                            $(".dingding span").hide();
                            $(".dingding span[id='track-name-" + dulait + "']").show();
                            
                            setTimeout(function(){
                                $(".dingding span[id='track-name-" + dulait + "']").removeClass("bongfade");
                            },quickie/2)
                        },quickie/2)
                    } else {
                        // if last song has ended
                        // and loop disabled
                        $(".vlay").show();
                        $(".vause").hide();
                    }// if next audio exists
                } else {
                    $(".vlay").show();
                    $(".vause").hide();
                }// if auto-next yes/no
                
                // if last song has finished playing
                // and user wants playlist loop
                // go back to first song
                if(!$(this).next("audio").length){
                    $(".current-song").each(function(){
                        $(".audiostaque audio:first").addClass("current-song");
                        $(this).removeClass("current-song")
                    })
                    
                    var n__n = $(".current-song")[0];
                    
                    if(n__n.paused){
                        n__n.play();
                        $(".vlay").hide();
                        $(".vause").show();
                    } else {
                        n__n.pause();
                        $(".vlay").show();
                        $(".vause").hide();
                    }
                    
                    $(".dingding span").addClass("bongfade");
                    
                    setTimeout(function(){
                        $(".dingding span").hide();
                        $(".dingding span[id='track-name-1']").show();
                        
                        setTimeout(function(){
                            $(".dingding span[id='track-name-1']").removeClass("bongfade");
                        },quickie/2)
                    },quickie/2)
                }
            });// onended
        });// audio each
        
        
        
        
        
        /*------- NEXT-CLICK -------*/
        $(".neet").click(function(){
            
            if($(".current-song").next("audio").length){
                $(".current-song").each(function(){
                    if($(this).next("audio").length){
                        $(this).next().addClass("current-song");
                        $(this).removeClass("current-song");
                    }
                    
                    var curdle = $(this).attr("id");
                    curdle = Number(curdle.replace(/\D/g,""));
                    
                    $(".dingding span").addClass("bongfade");
                    
                    setTimeout(function(){
                        $(".dingding span").hide();
                        $(".dingding span[id='track-name-" + Math.floor(curdle + 1) + "']").show();
                        
                        setTimeout(function(){
                            $(".dingding span[id='track-name-" + Math.floor(curdle + 1) + "']").removeClass("bongfade");
                        },quickie/2)
                    },quickie/2);
                    
                    var n__n = $(".audiostaque audio[id='track-src-" + Math.floor(curdle + 1) + "']")[0];
            
                    if(n__n.paused){
                        n__n.play();
                        $(".vlay").hide();
                        $(".vause").show();
                    } else {
                        n__n.pause();
                        $(".vlay").show();
                        $(".vause").hide();
                    }
                });
            } else {
                // if user is at last song, and wants to go forward
                // direct them to the 1st song
                var pestoh = $(".audiostaque audio[id='track-src-1']")[0];
                
                if(pestoh.paused){
                    pestoh.play();
                    $(".vlay").hide();
                    $(".vause").show();
                } else {
                    pestoh.pause();
                    $(".vlay").show();
                    $(".vause").hide();
                }
                
                $(".current-song").each(function(){
                    $(this).parent().find("audio:first").addClass("current-song");
                    $(this).removeClass("current-song")
                })
                
                $(".dingding span").addClass("bongfade");
                
                setTimeout(function(){
                    $(".dingding span").hide();
                    $(".dingding span[id='track-name-1']").show();
                    
                    setTimeout(function(){
                        $(".dingding span[id='track-name-1']").removeClass("bongfade");
                    },quickie/2)
                },quickie/2);
            }
        });//end next
        
        /*------- PREV-CLICK -------*/
        $(".peev").click(function(){
            
            if($(".current-song").prev("audio").length){
                $(".current-song").each(function(){
                    if($(this).prev("audio").length){
                        $(this).prev().addClass("current-song");
                        $(this).removeClass("current-song");
                    }
                    
                    var curdle = $(this).attr("id");
                    curdle = Number(curdle.replace(/\D/g,""));
                    
                    $(".dingding span").addClass("bongfade");
                    
                    setTimeout(function(){
                        $(".dingding span").hide();
                        $(".dingding span[id='track-name-" + Math.floor(curdle - 1) + "']").show();
                        
                        setTimeout(function(){
                            $(".dingding span[id='track-name-" + Math.floor(curdle - 1) + "']").removeClass("bongfade");
                        },quickie/2)
                    },quickie/2);
                    
                    var n__n = $(".audiostaque audio[id='track-src-" + Math.floor(curdle - 1) + "']")[0];
            
                    if(n__n.paused){
                        n__n.play();
                        $(".vlay").hide();
                        $(".vause").show();
                    } else {
                        n__n.pause();
                        $(".vlay").show();
                        $(".vause").hide();
                    }
                });
            } else {
                // if user is at song 1, and wants to go back
                // direct them to the last song
                var toto = $(".audiostaque audio").length;
                var lasto = $(".audiostaque audio[id='track-src-" + toto + "']")[0];
                
                if(lasto.paused){
                    lasto.play();
                    $(".vlay").hide();
                    $(".vause").show();
                } else {
                    lasto.pause();
                    $(".vlay").show();
                    $(".vause").hide();
                }
                
                $(".current-song").each(function(){
                    $(this).parent().find("audio:last").addClass("current-song");
                    $(this).removeClass("current-song")
                })
                
                $(".dingding span").addClass("bongfade");
                
                setTimeout(function(){
                    $(".dingding span").hide();
                    $(".dingding span[id='track-name-" + toto + "']").show();
                    
                    setTimeout(function(){
                        $(".dingding span[id='track-name-" + toto + "']").removeClass("bongfade");
                    },quickie/2)
                },quickie/2);
            }
        });//end next
        
        /*---- ONLY ALLOW 1 AUDIO TO PLAY AT A TIME ----*/
        var auxs = $(".audiostaque audio");
        
        auxs.on("play",function(){
            auxs.not(this).each(function(i, autres){
                autres.pause();
                autres.currentTime = 0;
            });
        });
    }// end if:showmusicplayer
    
    /*-----------------------------------------------------*/
    
    // add marquee to sidebar top text
    // if too long
    setTimeout(function(){
        $(".bubun span").each(function(){
            if($(this).width() > $(this).parent().width()){
                $(this).addClass("moomoo")
            }
        })
    },420)
    
    
    // show userguide prompt on customize page
    if(customize_page){
        $(".musubi").each(function(){
            $(this).show();
            $(this).html("Please read <a href=\'//docs.google.com/presentation/d/1Ntb6G6nYOz4NcLfHYB_JyJvZPpXQvaQbYCNLttMGREM/edit?usp=sharing\'>this guide</a> to get started!")
        })
    }
    
    // dismiss notif blocks on click
    var boompkin = getComputedStyle(document.documentElement)
               .getPropertyValue("--Sidebar-Notif-Dismiss-Speed-INT");
    
    
    $(".notif-wrap").click(function(){
        var that = this;
        $(this).addClass("calicow");
        
        setTimeout(function(){
            $(that).slideUp();
            
            // check if that was the last one standing
            if(!$(that).siblings(".notif-wrap:not(.calicow)").length){
                $(".moolait").removeClass("mooed")
            }
        },boompkin/2)
    })
    
    /*---- CUSTOM LINKS ----*/
    // remove cls if user doesn't have any
    $(".pengweng").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).parent(".ceels").remove()
        }
    })
    
    
    /*---- TRIM DESCRIPTION WHITESPACE ----*/
    $(".desc").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType === 3  && this.data.trim().length > 0
        }).wrap("<span>")
    })
    
    $(".desc p").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top",0)
        }
        
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })
    
    /*---- try to remove the src append ----*/
    $(".text-post").each(function(){
        // find npf_insts w/o caption and add .photo-origin
        $(this).find(".npf_inst:first").each(function(){
            if(!$(this).next().length){
                if($.trim($(this).prev("p").text()) == ""){
                    $(this).addClass("photo-origin");
                    $(this).prev("p").remove();
                    $(this).parent(".reblog-comment").prev(".reblog-head").remove();
                }
            }
        })
            
        if($(this).find(".reblog-wrap").length == "1"){
            if($(this).find(".el-npf-chat").length == "1"){
                $(this).find(".el-npf-chat").each(function(){
                    if($.trim($(this).prev("p").text()) == ""){
                        if(!$(this).next().length){
                            $(this).prev("p").remove();
                            $(this).parent(".reblog-comment").prev(".reblog-head").remove();
                        }
                    }
                })
            }
        }
    })
    
    /*---- convert npf post types (lol) ----*/
    // video
    $(".photo-origin").each(function(){
        if($(this).find(".tmblr-full[data-npf*='video']").length == "1"){
            // if npf video
            $(this).parents(".posts").find(".permatop .iconsax:first").attr("icon-name","video-2");
        } else {
            // if npf photo(s)
            $(this).parents(".posts").find(".permatop .iconsax:first").attr("icon-name","picture");
        }
        
        init_iconsax();
    })
    
    // audio (e.g. spotify, soundcloud)
    $("[post-type='text']:has([data-npf*='audio'])").each(function(){
        if($(this).find("[data-npf*='audio']").length == "1"){
            // if npf audio
            $(this).find(".permatop .iconsax:first").attr("icon-name","music");
        }
        
        init_iconsax();
    })
    
    // poll
    $("[post-type='text']:has([data-npf*='poll'])").each(function(){
        if($(this).find("[data-npf*='poll']").length == "1"){
            // if npf poll
            $(this).find(".permatop .iconsax:first").attr("icon-name","bar-graph-1");
        }
        
        init_iconsax();
    })
    
    // chat
    $(".el-npf-chat").each(function(){
        if(!$(this).siblings().length){
            $(this).parents(".posts").find(".permatop .iconsax:first").attr("icon-name","messages-1");
            init_iconsax();
        }
    })
});//end jquery / end ready
