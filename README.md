![Text banner that says "Radiance" in uppercase, and "Theme 09 by glenthemes" at the bottom](https://64.media.tumblr.com/b19e26bccb0428a18314a7695fc29a35/053578616f98941b-1f/s1280x1920/9d60c41132ce2e1d422c4a86879d337d861c0ef3.png)  
![Screenshot preview of the theme "Radiance" by glenthemes](https://64.media.tumblr.com/eeb1f10c2b1ebebb24df7aa873dcef74/053578616f98941b-8a/s1280x1920/0966254269ed2b23c6824e6ba958a49bcd9210d0.png)

**Theme no.:** 09  
**Theme name:** Radiance  
**Theme type:** Free / Tumblr use  
**Description:** A cute & minimal theme featuring Hinata Shouyou and soft aesthetics, with custom "notifications" in the sidebar and a fully functional music player.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-09-27](https://64.media.tumblr.com/d2d110c4600b86c47ff171f966c81779/tumblr_nva2pyzJXp1ubolzro1_1280.png)  
**Rework date:** 2021-12-05

**Post:** [glenthemes.tumblr.com/post/130041635074](https://glenthemes.tumblr.com/post/130041635074)  
**Preview:** [glenthpvs.tumblr.com/radiance](https://glenthpvs.tumblr.com/radiance)  
**Download:** [pastebin.com/4WVjbyF8](https://pastebin.com/4WVjbyF8)  
**Guide:** [docs.google.com/presentation/d/1Ntb6G6nYOz4NcLfHYB_JyJvZPpXQvaQbYCNLttMGREM/edit?usp=sharing](https://docs.google.com/presentation/d/1Ntb6G6nYOz4NcLfHYB_JyJvZPpXQvaQbYCNLttMGREM/edit?usp=sharing)  
**Credits:** [glencredits.tumblr.com/radiance](https://glencredits.tumblr.com/radiance)
